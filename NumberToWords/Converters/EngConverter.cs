﻿using System;
namespace NumberToWords
{
    public static class EngConverter
    {
        public static string[] unitsMap { get; set; } = { "zero", "one", "two", "three", "four", "five", "six",
            "seven", "eight", "nine", "ten", "eleven",
            "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };

        public static string[] tensMap { get; set; } = { "zero", "ten", "twenty", "thirty", "forty",
            "fifty", "sixty", "seventy", "eighty", "ninety" };

        public static string[] hundredsMap { get; set; } = { "", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion",
            "septillion", "octillion", "nonillion", "decillion", "undecillion", "duodecillion", "tredecillion", "quattuordecillion", "quindecillion",
            "sexdecillion", "septendecillion", "octodecillion", "novemdecillion",
            "vigintillion", "unvigintillion", "duovigintillion", "trevigintillion" };



        public static string NumberToWords(this long number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            for (int i = hundredsMap.Length; i >= 1; i--)
            {
                if ((number / (long)Math.Pow(10, 3 * i)) > 0)
                {
                    words += NumberToWords(number / (long)Math.Pow(10, 3 * i)) + " " + hundredsMap[i] + " ";
                    number %= (long)Math.Pow(10, 3 * i);
                }
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }
            return words;
        }

        public static string NumberToWords(this int number)
        {
            return NumberToWords((long)number);
        }

        public static string NumberToWords(this short number)
        {
            return NumberToWords((long)number);
        }
    }
}
